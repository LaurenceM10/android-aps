package techo.apps.isi.uca.com.android_aps.models;

/**
 * Created by macyarin on 10/4/18.
 */

public class EmailModel {
    private String emailAddress;
    private String tag;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
